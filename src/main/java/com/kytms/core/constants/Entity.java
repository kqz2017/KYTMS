package com.kytms.core.constants;

/**
 * 辽宁捷畅物流有限公司 -信息技术中心
 * 臧英明
 *
 * @author
 * @create 2017-11-23
 */
public abstract class Entity {
    /**树形根*/
    public static final String TREE_ROOT="root";
    /**生效*/
    public static final int ACTIVE=1;
    public static final int UNACTIVE=0;
}
